package testtask;

public interface Solution {
    public static int MAX_HEIGHT = 32000;
    public static int MAX_SIZE = 32000;

    public long calculateWaterAmount(int[] landscape);

}
