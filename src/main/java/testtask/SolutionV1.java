package testtask;

public class SolutionV1 implements Solution {


    public long calculateWaterAmount(int[] landscape) {
        if (landscape == null || landscape.length == 0 || landscape.length > MAX_SIZE) {
            return -1;
        }
        if (landscape.length < 3) {
            //no pits possible. validate and return.
            return checkTail(landscape, 0) ? 0 : -1;
        }
        //validate first
        if (!checkRange(landscape[0])) {
            return -1;
        }
        int maximum = findMaximum(landscape, 0);

        //true if on a range from 0 to maximum invalid data present
        if (landscape[maximum] > MAX_HEIGHT) {
            return -1;
        }
        if (maximum >= landscape.length - 2) {
            //no pits possible. validate after maximum and return.
            return checkTail(landscape, maximum + 1) ? 0 : -1;
        }
        long sum = 0;
        int current = maximum + 1;
        while (true) {
            int minimum = findMinimum(landscape, current);

            //true if on a range from 0 to minimum invalid data present
            if (landscape[minimum] < 0) {
                return -1;
            }

            //reached the end. no more pits possible (no maximums in future)
            if (minimum == landscape.length - 1) {
                return sum;
            }

            int floorLevel = landscape[minimum];
            int rightWall = minimum + 1;

            //moving to minimum+1. need to validate
            if (landscape[rightWall] > MAX_HEIGHT) {
                return -1;
            }

            int leftWall = minimum - 1;
            while (landscape[leftWall] == floorLevel) {
                leftWall--;
            }
            while (true) {
                //filling pit between left and right wall
                int topLevel = Math.min(landscape[leftWall], landscape[rightWall]);
                sum += (topLevel - floorLevel) * (rightWall - leftWall - 1);

                //now since everything below topLevel is filled, it will become a new floorLevel
                floorLevel = topLevel;

                //if right wall is same is floor, we need to check if there is something higher next, so pit can continue
                int prevRight = rightWall;
                int prevLeft = leftWall;
                if (landscape[rightWall] == floorLevel) {
                    //skip plato
                    while (rightWall < landscape.length - 1 && landscape[rightWall] == floorLevel) {
                        rightWall++;
                    }

                    if (landscape[rightWall] == floorLevel) {
                        //if landscape ends with plato, no more pits possible
                        return sum;
                    } else if (landscape[rightWall] < landscape[rightWall - 1]) {
                        //if after the plato we go down, then we filled the pit

                        //mark pit as filled
                        for (int i = prevLeft + 1; i < prevRight; i++) {
                            landscape[i] = floorLevel;
                        }

                        if (landscape[maximum] > floorLevel) {
                            //left maximum is higher than the pit top. it means current pit can be inside of a bigger pit
                            //maximum stays the same, next minimum is searched after the end of the pit
                            current = rightWall;
                        } else {
                            //last element of the plato is new maximum
                            //the next pit will be on the right from maximum
                            maximum = rightWall - 1;
                        }
                        //proceed to fill the next pit
                        break;
                    } else
                        //after the plato we go up. pit can continue. we move forward and validate new element
                        if (landscape[rightWall] > MAX_HEIGHT) {
                            return -1;
                        }
                }

                //if left wall is same is floor, we need to check if there is something higher before, so pit can continue
                if (landscape[leftWall] == floorLevel) {

                    if (landscape[maximum] == floorLevel) {
                        //check if left maximum is at floor (we are at plato of the same level as current max, cannot go higher).
                        //we filled the pit

                        //right wall must be higher than floor (otherwise we would break in right wall check)
                        //this situation means there are no more pits on the left from right wall

                        if (rightWall >= landscape.length - 2) {
                            //there is no room for new maximum on the right from the right wall. validate last unseen elements
                            return checkTail(landscape, rightWall + 1) ? sum : -1;
                        }

                        maximum = findMaximum(landscape, rightWall);
                        if (maximum > MAX_HEIGHT) {
                            //true if from rightWall to new maximum there are invalid data
                            return -1;
                        }
                        //fill te pit
                        for (int i = prevLeft + 1; i < prevRight; i++) {
                            landscape[i] = floorLevel;
                        }
                        //proceed with new maximum, find next minimum after it
                        current = maximum + 1;
                        break;
                    }
                    while (leftWall > maximum && landscape[leftWall] == floorLevel) {
                        leftWall--;
                    }

                    if (landscape[leftWall] == floorLevel) {
                    }
                }
            }
        }
    }

    private static boolean checkTail(int[] landscape, int from) {
        for (int i = from; i < landscape.length; i++) {
            if (!checkRange(landscape[i])) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkRange(int height) {
        return height >= 0 && height <= MAX_HEIGHT;
    }


    private static int findMaximum(int[] landscape, int i) {
        while (i < landscape.length - 1 && landscape[i] <= landscape[i + 1]) {
            i++;
        }
        return i;
    }

    private static int findMinimum(int[] landscape, int i) {
        while (i < landscape.length - 1 && landscape[i] >= landscape[i + 1]) {
            i++;
        }
        return i;
    }


}
