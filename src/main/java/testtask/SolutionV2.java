package testtask;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SolutionV2 implements Solution {


    public long calculateWaterAmount(int[] landscape) {
        if (landscape == null || landscape.length == 0 || landscape.length > MAX_SIZE) {
            return -1;
        }
        for (int i = 0; i < landscape.length; i++) {
            if (landscape[i] < 0 || landscape[i] > MAX_HEIGHT) {
                return -1;
            }
        }
        if (landscape.length < 3) {
            return 0;
        }

        List<int[]> toSort = new ArrayList<int[]>();
        for (int i = 0; i < landscape.length; i++) {
            toSort.add(new int[]{landscape[i], i});
        }

        Collections.sort(toSort, new Comparator<int[]>() {
            public int compare(int[] o1, int[] o2) {
                int d = o1[0] - o2[0];
                return d == 0 ? o1[1] - o2[1] : -d;
            }
        });

        int start = 0;
        int end = 1;
        int currentHeight = toSort.get(start)[0];
        while (end < toSort.size() && toSort.get(end)[0] == currentHeight) {
            end++;
        }
        if (end != toSort.size() - 1 || toSort.get(end)[0] != currentHeight) {
            end--;
        }
        int left = toSort.get(start)[1];
        int right = toSort.get(end)[1];

        long sum = 0;
        for (int i = left + 1; i < right; i++) {
            sum += currentHeight - landscape[i];
        }
        start = end + 1;
        while (start < toSort.size() && (left > 1 || right < landscape.length - 2)) {

            end = start + 1;
            currentHeight = toSort.get(start)[0];
            while (end < toSort.size() && toSort.get(end)[0] == currentHeight) {
                end++;
            }
            if (end != toSort.size() - 1 || toSort.get(end)[0] != currentHeight) {
                end--;
            }
            int newLeft = toSort.get(start)[1];
            int newRight = toSort.get(end)[1];

            if (newLeft < left) {
                for (int i = newLeft + 1; i < left; i++) {
                    sum += currentHeight - landscape[i];
                }
                left = newLeft;
            }
            if (newRight > right) {
                for (int i = right + 1; i < newRight; i++) {
                    sum += currentHeight - landscape[i];
                }
                right = newRight;
            }
            start = end + 1;
        }
        return sum;
    }


}
