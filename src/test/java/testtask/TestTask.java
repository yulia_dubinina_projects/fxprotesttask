package testtask;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class TestTask {

    @Parameters
    public static Collection<Solution> data() {
        return Arrays.asList(new Solution[]{new SolutionV1(), new SolutionV2()});
    }

    @Parameter
    public Solution solution;


    @Test
    public void checkWrongHeight() {
        //first wrong
        Assert.assertEquals(-1, solution.calculateWaterAmount(new int[]{-1, 2, 3, 4}));
        Assert.assertEquals(-1, solution.calculateWaterAmount(new int[]{Solution.MAX_HEIGHT + 1, 2, 3, 4}));
        //on find max
        Assert.assertEquals(-1, solution.calculateWaterAmount(new int[]{1, 2, 3, Solution.MAX_HEIGHT + 1}));
        //on find min
        Assert.assertEquals(-1, solution.calculateWaterAmount(new int[]{3, 2, 1, -1}));
        //last wrong
        Assert.assertEquals(-1, solution.calculateWaterAmount(new int[]{1, 2, 3, -1}));
        Assert.assertEquals(-1, solution.calculateWaterAmount(new int[]{3, 2, 1, 2, 3, -1}));
        //on right wall
        Assert.assertEquals(-1, solution.calculateWaterAmount(new int[]{4, 3, 2, 1, 2, 3, Solution.MAX_HEIGHT + 1}));
    }

    @Test
    public void checkWrongLength() {
        Assert.assertEquals(-1, solution.calculateWaterAmount(new int[]{}));
        Assert.assertEquals(-1, solution.calculateWaterAmount(new int[Solution.MAX_SIZE + 1]));
    }

    @Test
    public void checkNull() {
        Assert.assertEquals(-1, solution.calculateWaterAmount(null));
    }

    @Test
    public void testAmount() {
        //example
        Assert.assertEquals(9, solution.calculateWaterAmount(new int[]{5, 2, 3, 4, 5, 4, 0, 3, 1}));
        //big
        int[] test = new int[Solution.MAX_SIZE];
        test[0] = Solution.MAX_HEIGHT;
        test[test.length - 1] = Solution.MAX_HEIGHT;
        Assert.assertEquals(Solution.MAX_HEIGHT * (Solution.MAX_SIZE - 2), solution.calculateWaterAmount(test));
        //flat
        Assert.assertEquals(0, solution.calculateWaterAmount(new int[]{1, 1, 1, 1}));
        //hill
        Assert.assertEquals(0, solution.calculateWaterAmount(new int[]{0, 0, 0, 1, 1, 3, 4, 4, 4, 4, 8, 8, 7, 6, 6, 6, 6, 4, 3, 0, 0, 0}));
        //other
        Assert.assertEquals(1, solution.calculateWaterAmount(new int[]{1, 1, 0, 1, 1}));
        Assert.assertEquals(5, solution.calculateWaterAmount(new int[]{1, 1, 0, 0, 0, 0, 0, 1, 1}));
        Assert.assertEquals(20, solution.calculateWaterAmount(new int[]{1, 2, 3, 4, 0, 0, 0, 0, 0, 4, 3, 2, 1}));
        Assert.assertEquals(5, solution.calculateWaterAmount(new int[]{1, 2, 1, 2, 10, 7, 6, 5, 7, 4, 5, 2, 1}));
        Assert.assertEquals(12, solution.calculateWaterAmount(new int[]{1, 2, 1, 2, 10, 7, 6, 5, 7, 4, 8, 2, 1}));
    }
}
