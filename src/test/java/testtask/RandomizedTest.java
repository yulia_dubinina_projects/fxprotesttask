package testtask;

import org.junit.Assert;
import org.junit.Test;

public class RandomizedTest {

    private long runTest(Solution solution, int[] landscape) {
        long time = System.currentTimeMillis();
        long res = 0;
        for (int i = 0; i < 10000; i++) {
            res = solution.calculateWaterAmount(landscape);
        }
        System.out.println("Time: " + (System.currentTimeMillis() - time) + "ms");
        return res;
    }

    @Test
    public void randomizedTest() {
        SolutionV1 v1 = new SolutionV1();
        SolutionV2 v2 = new SolutionV2();

        int[] landscape = new int[Solution.MAX_SIZE];
        for (int i = 0; i < landscape.length; i++) {
            landscape[i] = (int) Math.round(Math.random() * Solution.MAX_HEIGHT);
        }

        System.out.println("Testing version 1:");
        long res1 = runTest(v1, landscape);

        System.out.println("Testing version 2:");
        long res2 = runTest(v2, landscape);

        Assert.assertEquals(res1, res2);
    }

}
